# encoding: utf-8

import socket

from fabric import colors
from fabric.api import *

env.user = 'jelly'
env.password = 'Jelly001!'

VIRTUALENV_DIR = '/home/jelly/KTInfluenza_2nd'
REMOTE_WORKDIR = '/home/jelly/KTInfluenza_2nd/ktinfluenza_2nd'
ACTIVATE = 'source %s/bin/activate' % VIRTUALENV_DIR

env.roledefs = {
    'app': ['jelly@173.192.142.34', ],
    'app_qa': ['jelly@173.192.142.35', ],
}

def app():
    env.roles = ['app']

def app_qa():
    env.roles = ['app_qa']

def createsuperuser():
    puts(colors.green("관리자 계정이 생성됩니다. 사용하실 비밀번호를 입력해주세요."))
    local('./manage.py createsuperuser --email=""')

def runserver(ip=socket.gethostbyname(socket.gethostname()), port=8000):
    local('./manage.py runserver_plus %s:%s' % (ip, port))

def shell():
    local('./manage.py shell_plus')

def syncdb():
    puts(colors.green("DB Sync 작업을 시작합니다."))
    local('./manage.py syncdb --noinput')

def initial():
    puts(colors.green("스키마 마이그레이션 Inital 작업을 시작합니다."))
    local('./manage.py schemamigration apps --initial')

def migration():
    puts(colors.green("스키마 마이그레이션 자동 추가 작업을 시작합니다."))
    local('./manage.py schemamigration apps --auto')

def migrate():
    puts(colors.green("마이그레이트 작업을 시작합니다."))
    local('./manage.py migrate --all')

def dbreset():
    puts(colors.green("DB 초기화 작업을 시작합니다."))
    local('rm -rf container/*.db')
    syncdb(mode)
    migrate(mode)
    createsuperuser(mode)

def collect():
    with cd(REMOTE_WORKDIR), prefix(ACTIVATE):
        run('./manage.py collectstatic -i flags* -i cache* -i *.html -i *.htm --noinput')

def deploy():
    with cd(REMOTE_WORKDIR), prefix(ACTIVATE):
        puts(colors.blue("Updating project folder -> %s:%s") %
             (env.host, env.port))

        run('git stash save --keep-index', quiet=True)
        run('git stash drop', quiet=True)

        output_text = run('git pull origin')
        puts(colors.blue(output_text))

        puts(colors.blue("프론트엔드 의존성 패키지를 업데이트합니다."))
        run('bower install --production')

        execute(collect)

        run('pip install -r requirements.txt')
        sudo('supervisorctl restart all')
