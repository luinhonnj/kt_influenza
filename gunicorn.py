import multiprocessing

bind = "127.0.0.1:8001"
workers = multiprocessing.cpu_count() * 2 + 1
#worker_class = 'gevent'
max_requests = 1000
timeout = 600

# Server Mechanics
preload_app = True
daemon = False  # this allows supervisor to correctly handle gunicorn
pidfile = 'gunicorn.pid'

# Log
errorlog = 'logs/gunicorn.log'
loglevel = 'debug'
