# -*- coding: utf-8 -*-
from django.conf import settings
from os.path import join, normpath
from django.http import HttpResponse
from django.shortcuts import render,redirect
from django.core.urlresolvers import reverse
from django.views.decorators.csrf import csrf_exempt
from django.db import connection
from datetime import date
from .models import *
import json

def index(request):
    cursor = connection.cursor()
    cursor.execute("SELECT * FROM estimate GROUP BY basis_date ORDER BY basis_date DESC")
    dates = cursor.fetchall()
    years = []
    months = []
    days = []
    year = month = day = 0
    for item in dates :
        if year != item[11].year :
            year = item[11].year
            years.append(year)
        if month != item[11].month :
            month = item[11].month
            months.append(month)
        if day != item[11].day :
            day = item[11].day
            days.append(day)

    context = { 'years' : reversed(years), 'months' : reversed(months), 'days' : reversed(days) }
    return render(request, 'base.html', context)

def get_risks(request):
    ref = request.GET.get('refdate', None)
    cursor = connection.cursor()
    cursor.execute("SELECT * FROM code_with_risk_view WHERE basis_date = %s AND `risk` > 1", [ref])
    risks = cursor.fetchall()
    risk_data = []
    for item in risks:
        risk_data.append({
            'risk':int(item[6]),
            'si_code':int(item[0]),
            'gu_code':int(item[2]),
            'dong_code':int(item[4])
        })
    return HttpResponse(json.dumps(risk_data),"application/json")

def get_farms(request):
    ref = request.GET.get('refdate',None)
    level = request.GET.get('level','si')
    parent = request.GET.get('parent', None)
    bd = ref.split('-')
    cursor = connection.cursor()
    if level == 'si':
        cursor.execute("SELECT dong_code, farm_id, lnt,lat FROM farm_gps_dong_code")
    elif level == 'gu':
        cursor.execute("SELECT * FROM test_view WHERE basis_date=%s AND si_code=%s", [ref,parent])
    elif level == 'dong':
        cursor.execute("SELECT * FROM test_view WHERE basis_date=%s AND gu_code=%s", [ref,parent])
    farms = cursor.fetchall()
    
    farm_data = []
    for item in farms:
        if level == 'si':
            farm_data.append({
                'dong_code':int(item[0]),
                'farm_code':int(item[1]),
                'lnt':str(item[2]),
                'lat':str(item[3])
            })
        else :
            is_hot = False
            if item[10] != None:
                d = str(item[10]).split('-')
                days = date(int(bd[0]),int(bd[1]),int(bd[2])) - date(int(d[0]),int(d[1]),int(d[2]))
                if int(days.days) < 6 :
                    is_hot = True
            farm_data.append({
                # 'si_code':item.si_code,
                # 'gu_code':item.gu_code,
                # 'dong_code':item.dong_code,
                # 'farm_code':item.farm_code,
                # 'lnt':item.lnt,
                # 'lat':item.lat
                'si_code':int(item[0]),
                'gu_code':int(item[1]),
                'dong_code': int(item[2]),
                'farm_code':int(item[4]),
                'hot':is_hot,
                'lnt':str(item[12]),
                'lat':str(item[13]),
            })

    return HttpResponse(json.dumps(farm_data),"application/json")

def get_estimate(request):
    dong_code = request.GET.get('dong_code',None)
    ref = request.GET.get('refdate',None)
    bd = ref.split('-')
    region_name = Regions.objects.filter(dong_code=dong_code)[0]
    cursor = connection.cursor()

    cursor.execute("SELECT risk FROM estimate_risk_view WHERE dong_code = %s AND basis_date = %s",[dong_code,ref])
    risk = cursor.fetchone()

    datas = Estimate.objects.filter(dong_code=dong_code,basis_date=ref)
    # cursor.execute("SELECT farm_code, farm_type, car_type, car_reg, farm_code_from, farm_type_from, report_date  FROM estimate WHERE dong_code = %s AND basis_date = %s",[dong_code,ref])
    # datas = cursor.fetchall()

    if not datas :
        data = { 'success': False }
    else :
        data = { 'success': True,
            'basis_date': ref,
            'risk' : int(risk[0]),
            'town': region_name.si_name+' '+region_name.gu_name+' '+region_name.dong_name,
            'fowl' : []
        }

    farmcode = 0
    fowl = []
    fowllow = []
    valid_cnt = 0
    for item in datas:
        if item.car_type != '-':
            valid_cnt += 1
        if farmcode != int(item.farm_code):
            farmcode = int(item.farm_code)
            if item.car_type == '-':
                fowllow.append({ 'farm_code': str(farmcode), 'title' : item.farm_type, 'child' : [] })
            else :
                fowl.append({ 'farm_code': str(farmcode), 'title' : item.farm_type, 'child' : [] })
            if item.car_type == '-':
                fowllow[len(fowllow)-1]['child'].append({ 'mediator' : item.car_type, 'mediator2':item.car_reg, 'epidem':str(item.farm_code_from), 'epidem2':item.farm_type_from,'reported':'-' })
            else :
                is_hot = False
                if item.report_date != None:
                    d = str(item.report_date).split('-')
                    days = date(int(bd[0]),int(bd[1]),int(bd[2])) - date(int(d[0]),int(d[1]),int(d[2]))
                    if int(days.days) < 6 :
                        is_hot = True
                fowl[len(fowl)-1]['child'].append({ 'mediator' : item.car_type, 'mediator2':item.car_reg, 'epidem':str(item.farm_code_from), 'epidem2':item.farm_type_from,'reported':str(item.report_date),'hot':is_hot })
        # if item[2] != '-':
        #     valid_cnt += 1
        # if farmcode != int(item[0]):
        #     farmcode = int(item[0])
        #     if item[2] == '-':
        #         fowllow.append({ 'farm_code': str(farmcode), 'title' : item[1], 'child' : [] })
        #     else :
        #         fowl.append({ 'farm_code': str(farmcode), 'title' : item[1], 'child' : [] })
        # if item[2] == '-':
        #     fowllow[len(fowllow)-1]['child'].append({ 'mediator' : item[2], 'mediator2':item[3], 'epidem':str(item[4]), 'epidem2':item[5],'reported':item[6] })
        # else :
        #     fowl[len(fowl)-1]['child'].append({ 'mediator' : item[2], 'mediator2':item[3], 'epidem':str(item[4]), 'epidem2':item[5],'reported':item[6] })

    data['fowl'] = fowl+fowllow
    data['valid_cnt'] = valid_cnt

    return HttpResponse(json.dumps(data),"application/json")

def download(request):
    if 'basis_date' in request.GET:
        basis_date = request.GET['basis_date']
    else:
        return HttpResponse('Required parameter missing: basis_date', content_type='text/plain')
    
    if 'dong_code' in request.GET:
        dong_code = request.GET['dong_code']
    else:
        dong_code = ''

    response = HttpResponse(content_type='text/csv')

    cursor = connection.cursor()

    if dong_code:
        response['Content-Disposition'] = 'attachment; filename="DATA_%s_%s.csv"' % (basis_date, dong_code)
        cursor.execute("SELECT dong_code, dong_name, risk, farm_code, farm_type, \
            car_type, car_reg, farm_code_from, farm_type_from, report_date \
            FROM estimate WHERE basis_date=%s and dong_code=%s", [basis_date, dong_code])
    else:
        response['Content-Disposition'] = 'attachment; filename="DATA_%s.csv"' % (basis_date)
        cursor.execute("SELECT dong_code, dong_name, risk, farm_code, farm_type, \
            car_type, car_reg, farm_code_from, farm_type_from, report_date \
            FROM estimate WHERE basis_date=%s", [basis_date])

    result_estimate = cursor.fetchall()

    csv_text = u'\uFEFF'
    csv_text += u'dong_code,dong_name,risk,farm_code,farm_type,car_type,car_reg,farm_code_from,farm_type_from,report_date\n'
    csv_text += u'\n'.join([','.join(unicode(item) for item in inner) for inner in result_estimate])

    response.write(csv_text.encode('utf-8'))

    return response
