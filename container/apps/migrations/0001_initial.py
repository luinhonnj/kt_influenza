# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Estimate',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('dong_code', models.IntegerField(default=0)),
                ('dong_name', models.CharField(max_length=15)),
                ('risk', models.IntegerField(default=0)),
                ('farm_code', models.IntegerField(default=0)),
                ('farm_type', models.CharField(max_length=30)),
                ('car_type', models.CharField(max_length=30)),
                ('car_reg', models.CharField(max_length=30)),
                ('farm_code_from', models.IntegerField(default=0)),
                ('farm_type_from', models.CharField(max_length=30)),
                ('report_date', models.CharField(max_length=20)),
                ('basis_date', models.CharField(max_length=20)),
            ],
            options={
                'db_table': 'estimate',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='FarmGPS',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('code', models.IntegerField(default=0)),
                ('farm_type', models.CharField(max_length=30)),
                ('lnt', models.CharField(max_length=30)),
                ('lat', models.CharField(max_length=30)),
            ],
            options={
                'db_table': 'farm_gps',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='RegionDong',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('code', models.IntegerField(default=0)),
                ('gu_code', models.IntegerField(default=0)),
                ('name', models.CharField(max_length=15)),
                ('path', models.TextField()),
            ],
            options={
                'db_table': 'region_dong',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='RegionGu',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('code', models.IntegerField(default=0)),
                ('si_code', models.IntegerField(default=0)),
                ('name', models.CharField(max_length=15)),
                ('path', models.TextField()),
            ],
            options={
                'db_table': 'region_gu',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='RegionSi',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('code', models.IntegerField(default=0)),
                ('name', models.CharField(max_length=15)),
                ('path', models.TextField()),
            ],
            options={
                'db_table': 'region_si',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Risk',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('si_code', models.IntegerField(default=0)),
                ('gu_code', models.IntegerField(default=0)),
                ('dong_code', models.IntegerField(default=0)),
                ('level', models.IntegerField(default=0)),
                ('refdate', models.CharField(max_length=30)),
            ],
            options={
                'db_table': 'risk',
            },
            bases=(models.Model,),
        ),
    ]
