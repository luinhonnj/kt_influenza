# -*- coding: utf-8 -*
from django.db import models 
from django.utils.translation import ugettext_lazy as _

class RegionSi(models.Model):
    code = models.IntegerField(default=0)
    name = models.CharField(max_length=15)

    class Meta:
        db_table = 'region_si'

class RegionGu(models.Model):
    code = models.IntegerField(default=0)
    si_code = models.IntegerField(default=0)
    name = models.CharField(max_length=15)

    class Meta:
        db_table = 'region_gu'

class RegionDong(models.Model):
    code = models.IntegerField(default=0)
    gu_code = models.IntegerField(default=0)
    name = models.CharField(max_length=15)

    class Meta:
        db_table = 'region_dong'

class Regions(models.Model):
    si_code = models.IntegerField(default=0)
    si_name = models.CharField(max_length=15)
    gu_code = models.IntegerField(default=0)
    gu_name = models.CharField(max_length=15)
    dong_code = models.IntegerField(default=0)
    dong_name = models.CharField(max_length=15)

    class Meta:
        db_table = 'region_sgd'

class FarmGPS(models.Model):
    code = models.IntegerField(default=0)
    farm_type = models.CharField(max_length=30)
    lnt = models.CharField(max_length=30)
    lat = models.CharField(max_length=30)

    class Meta:
        db_table = 'farm_gps'

class Estimate(models.Model):
    dong_code = models.IntegerField(default=0)
    dong_name = models.CharField(max_length=15)
    risk = models.IntegerField(default=0)
    farm_code = models.IntegerField(default=0)
    farm_type = models.CharField(max_length=30)
    car_type = models.CharField(max_length=30)
    car_reg = models.CharField(max_length=30)
    farm_code_from = models.IntegerField(default=0)
    farm_type_from = models.CharField(max_length=30)
    report_date = models.DateField()
    basis_date = models.DateField()

    class Meta:
        db_table = 'estimate'

class Withfarm(models.Model):
    si_code = models.IntegerField(default=0)
    si_name = models.CharField(max_length=15)
    gu_code = models.IntegerField(default=0)
    gu_name = models.CharField(max_length=15)
    dong_code = models.IntegerField(default=0)
    dong_name = models.CharField(max_length=15)
    farm_code = models.IntegerField(default=0)
    lnt = models.CharField(max_length=30)
    lat = models.CharField(max_length=30)
    risk = models.IntegerField(default=0)
    basis_date = models.CharField(max_length=20)

    class Meta:
        db_table = 'test_view'