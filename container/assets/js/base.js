$.ajaxSetup({ async : true, type : 'get' });
var isMobile = (navigator.userAgent.toLowerCase().indexOf('android') > -1) || (navigator.userAgent.match(/(iPod|iPhone|iPad|BlackBerry|Windows Phone|iemobile)/)),
spinner, spinnerOption = {lines:20,length:30,width:5,radius:15,rotate:0,color:'#3db0c6',speed:1,trail:90,shadow:true,hwaccel:false,className:'spinner',zIndex:4999,top:'50px',left:'50px'},
leadingZeros = function(n, digits){
  var zero = '';
  n = n.toString();
  if (n.length < digits) {
    for (var i = 0; i < digits - n.length; i++)
      zero += '0';
  }
  return zero + n;
},
projection, path, newscale, map = $('#mapCanvas'), slider = $('sl'), info = $('#info'), farm, mapWidth, mapHeight, is_farm = false, rendered_farm = false, q = null, ajaxcall_info = null,
risks, $region_dong = {}, risk_dong = {}, gu_l, d2_code, dong_l, d3_code, level = 'si', parent = null, refdate,
toggleFarm = function(checked){
    if(checked){
        is_farm = true;
        $('#farm').show();
        if(!rendered_farm) farmRender();
    } else {
        is_farm = false;
        $('#farm').hide();
    }
},
riskLayerHandler = function(){
    var checked = new Array(),where,style={ style : 'fill:#eee' };
    $.each($('.switches input:checked'),function(){
        checked.push(Number($(this).val()));
    });

    for(i=5; i>0; i--){
        var t = $('.risk'+i+' path');
        if(_.indexOf(checked,i) < 0) {
            if(level == 'si') {
                if(i==1) $('#region g path').attr(style);
                else t.attr(style);
            } else t.hide();
        } else {
            if(level == 'si') {
                if(i==1) $('#region g path').removeAttr('style');
                else t.removeAttr('style');
            } else t.show();
        }
    }
    // if(_.indexOf(checked,1) < 0) {
    //     $('#region g path').attr(style);
    //     $('.risk1 path').attr(style); 
    // }
    //     //$('#region path').attr('style','fill : #eee');
    // else {
    //     $('#region g path').removeAttr('style');
    //     $('.risk1 path').removeAttr('style');//$('#region path').attr('style','fill:#fef0d9');
    // }

},
setlocation = function(level){
    if(level == 'si') $('#d2,#d3').empty();
    if(level == 'gu'){
        $('#d3').empty();
        $('#d2').html(' > <a href="#d2">'+gu_l+'</a>');
    }
    if(level == 'dong') $('#d3').html(' > '+dong_l);
},
ajaxurl = function(){
    switch(level){
        case 'si': return STATIC_URL+'region_si.json'; break;
        case 'gu': return STATIC_URL+'region_gu.json'; break;
        case 'dong': return STATIC_URL+'region_dong.json'; break;
    }    
},
render = function(option){
    level = option.level;
    parent = option.parent;
    rendered_farm = false;

    d3.json(ajaxurl(), function(error, region) {
        if(error) return console.error(error);

        if(level != 'si'){
            var geometries = region.objects.regions.geometries;
            if(level == 'gu'){
                var filtered = _.filter(geometries, function(d){ return d.properties.SI_CODE == option.parent});    
                region.objects.regions.geometries = filtered;
                risk_dong = JSON.parse(JSON.stringify($region_dong));
                geometries = risk_dong.objects.regions.geometries;
                filtered = _.filter(geometries, function(d){ return d.properties.SI_CODE == option.parent });
            }
            if(level == 'dong'){
                var filtered = _.filter(geometries, function(d){ return d.properties.GU_CODE == option.parent});
                region.objects.regions.geometries = filtered;
                risk_dong = JSON.parse(JSON.stringify($region_dong));
                geometries = risk_dong.objects.regions.geometries;
                filtered = _.filter(geometries, function(d){ return d.properties.GU_CODE == option.parent });
            }
            risk_dong.objects.regions.geometries = filtered;   
        }
        var canvas = d3.select('#mapCanvas');
        var container = canvas.append('svg:svg').attr({'class':level, 'width' : mapWidth, 'height': mapHeight, 'viewBox':'0 0 '+mapWidth+' '+mapHeight }).append('g');
        var lpath = d3.geo.path();
        svg = container.append('g').attr({'id':'region'});
        svg.selectAll('#region').data(topojson.feature(region, region.objects.regions).features).enter()
            .append('g')
            .attr({
                id : function(d){ 
                    switch(level){
                        case 'si' : return d.properties.SI_CODE; break;
                        case 'gu' : return d.properties.GU_CODE; break;
                        case 'dong' : return d.properties.DONG_CODE; break;
                    }
                },
                name:function(d){
                    switch(level){
                        case 'si' : return d.properties.SI_NAME; break;
                        case 'gu' : return d.properties.GU_NAME; break;
                        case 'dong' : return d.properties.DONG_NAME; break;
                    }
                },
                centroid:function(d){
                    var centroid = path.centroid(d),
                    x=centroid[0], y=centroid[1];
                    return x+','+y;
                }
            })
            .on('click', function(){
                console.log(this.id);
                if(option.level == 'dong') openInfo({
                    id : this.id
                });
                else if(option.level == 'gu') {
                    dong_l = $(this).attr('name');
                    d2_code = option.parent;
                    preRender();
                    render({level:'dong',parent:this.id});
                }
                else {
                    gu_l = $(this).attr('name');
                    preRender();
                    render({level:'gu',parent:this.id});
                }
            })
            .append("path").attr({'d':path});

        risk = container.append('g').attr({'id':'risk'});
        risk.selectAll('#risk').data(topojson.feature(risk_dong, risk_dong.objects.regions).features).enter()
            .append('g')
            .attr({
                'data-idx' : function(d){ 
                    switch(level){
                        case 'si' : return d.properties.SI_CODE; break;
                        case 'gu' : return d.properties.GU_CODE; break;
                        case 'dong' : return d.properties.DONG_CODE; break;
                    }
                },
                'class' : function(d){
                    var risk_level = _.findWhere(risks, {'dong_code' : Number(d.properties.DONG_CODE) });
                    if(risk_level != undefined) return 'risk'+risk_level.risk;
                    else return 'risk1';
                }
            })
            .on('click', function(){
                if(option.level == 'dong') openInfo({
                    id : $(this).attr('data-idx')
                });
                else if(option.level == 'gu') {
                    dong_l = $('#'+$(this).attr('data-idx')).attr('name');
                    d2_code = option.parent;
                    preRender();
                    render({level:'dong',parent:Number($(this).attr('data-idx'))});
                }
                else {
                    gu_l = $('#'+$(this).attr('data-idx')).attr('name');
                    preRender();
                    render({level:'gu',parent:Number($(this).attr('data-idx'))});
                }
            })
            .append('path').attr({d:path});

        farm = container.append('g').attr({id:'farm', style:function(){
            if(is_farm) return 'display:block';
            else return 'display:none';
        }});

        var fx = 0,fy = 0,offset = container[0][0].getBBox();
        var er = level == 'si' ? 0.95 : 0.95;
        // if(mapWidth > mapHeight) newscale = mapHeight/offset.height * er;
        // else newscale = mapWidth/offset.width * er;
        newscale = (mapWidth/offset.width < mapHeight/offset.height)? mapWidth/offset.width * er : mapHeight/offset.height * er;        
        fx = (mapWidth-(offset.width*newscale))/2-offset.x*newscale;
        fy = (mapHeight-(offset.height*newscale))/2-offset.y*newscale;
        container.attr({transform:'translate('+String(fx)+','+String(fy)+'),scale('+newscale+')'}); 

        riskLayerHandler();
        setlocation(option.level);

        if(is_farm) farmRender();

        svg.selectAll('#region g')
            .each(function(){
                var group = d3.select(this);
                elm = group[0][0];
                container.append('text').text($(this).attr('name')).attr({
                    'data-idx': this.id,
                    'transform':function(){
                        var scale = 1.3/newscale ;
                        // switch(level){
                        //     case 'si' : scale = 1; break;
                        //     case 'gu' : scale = 0.4; break;
                        //     case 'dong' : scale = 0.1; break;
                        // }
                        var cent = $(elm).attr('centroid').split(',');
                        if($(this).attr('data-idx') == '41') cent[1] = Number(cent[1])+ 15;
                        return trans = 'translate('+ cent[0] + ',' + cent[1] + '),scale('+scale+')';
                    }
                })
                .on('click',function(){
                    if(option.level == 'dong') openInfo({
                        id : $(this).attr('data-idx')
                    });
                    else if(option.level == 'gu') {
                        dong_l = $('#'+$(this).attr('data-idx')).attr('name');
                        d2_code = option.parent;
                        preRender();
                        render({level:'dong',parent:Number($(this).attr('data-idx'))});
                    }
                    else {
                        gu_l = $('#'+$(this).attr('data-idx')).attr('name');
                        preRender();
                        render({level:'gu',parent:Number($(this).attr('data-idx'))});
                    }
                    return false;
                });
            });
        if(level == 'dong'){
            farm.selectAll('circle').on('click',function(){
                console.log(this);
            });
        }
        spinner.stop();
        spinner=null;
        $('#spin').remove();
    });
},

Binding = function(){
    $(window).bind('resize',function(){
        mapHeight = $(window).height() - 84;
        $('#container,#info').css('height',mapHeight);
    });
    $(document).bind('click',function(e){
        var t = $(e.target);
        if(!t.closest('#info').length && !t.closest('#googft-legend').length && !t.closest('#header').length){
            if(info.hasClass('active')) {
                if(t.closest('g g g').length>0 || t.closest('g text').length>0){
                    info.removeClass('active');
                } else closeInfo();
            }
        }
    });
    $('body')
    .delegate('#d1','click',function(){
        map.empty();
        closeInfo();
        if(level!='si') getRisks({level:'si'});
        return false;        
    })
    .delegate('#d2','click',function(){
        map.empty();
        closeInfo();
        if(level!='gu') getRisks({level:'gu',parent:d2_code});
        return false;        
    })
    .delegate('#info .closer','click',function(){
        closeInfo();
        return false;
    })
    .delegate('#allcheck','change',function(){
        var ext = $('.switches input:checked').length>0 ? true : false;
        if($(this).is(':checked') && !ext){
            $('.switches input').prop('checked', this.checked);
        } else {
            if($(this).is(':checked')){
                if(!ext) $('.switches input').removeAttr('checked');
                else $('.switches input').prop('checked', this.checked);
            } else {
                if(ext) $('.switches input').removeAttr('checked');
                else $('.switches input').prop('checked', this.checked);            
            }            
        }
        riskLayerHandler();        
    })
    .delegate('#filter select','change',function(){
        refdate = $('#s_year').val()+'-'+leadingZeros($('#s_month').val(),2)+'-'+leadingZeros($('#s_day').val(),2);
    })
    .delegate('#filter .show','click',function(){
        preRender();
        getRisks({level:'si'});
        return false;
    })
    .delegate('#filter .save','click',function(){
        document.location = '/download?basis_date='+refdate;
        return false;
    })
    .delegate('.switches input','change',function(){
        riskLayerHandler();
    });

    $('#option-farm .toggles').toggles({
        on:false,
        width:60
    }).on('toggle',function(e,active){
        toggleFarm(active);
    });

},

closeInfo = function(){
    info.removeClass('active');
    map.removeClass('shift');
},

preRender = function(){
    if(spinner!=null){spinner.stop();spinner=null;}
    map.empty();
    $('<div id="spin"/>').appendTo(map);
    spinner = new Spinner(spinnerOption).spin(document.getElementById('spin'));
},
farmRender = function(){
    if(q!=null) { q.abort(); q=null; }
    $('#farming').show();
    q = $.ajax({
        url : '/ajax/get/farms?refdate='+refdate+'&level='+level+'&parent='+parent,
        dataType : 'json',
        success : function(r){
            $('#farm').empty();
            r.forEach(function(v){
                var xy = projection([v.lnt,v.lat]);
                farm.append('circle').attr({
                    farm:v.farm_code,
                    cx: xy[0],
                    cy: xy[1],
                    r: function(){
                        if(level == 'si') return 1.2/newscale;
                        if(level == 'gu') return 1.6/newscale;
                        if(level == 'dong') return 2.2/newscale;
                    },
                    class: function(){
                        if(v.hot) return 'hot';
                    }
                });
            });
            $('#farming').hide();
            rendered_farm = true;
        }
    });
},
openInfo = function(opt){
    if(ajaxcall_info!=null){ 
        ajaxcall_info.abort(); ajaxcall_info = null;
        try { spinner.stop(); } catch(e){}
    }
    $('<div id="spin"/>').appendTo(map);
    spinner = new Spinner(spinnerOption).spin(document.getElementById('spin'));
    ajaxcall_info = $.ajax({
        url : '/ajax/get/estimate',
        type : 'get',
        data : { dong_code : opt.id , refdate : refdate},
        success : function(r){
            spinner.stop();
            $('#spin').remove();
            if(!r.success){
            } else {
                _.extend(r, opt);
                info.html(_.template($('#infoTemplate').html())(r));
                if(!info.hasClass('active')){
                    info.addClass('active');
                    $('#mapCanvas').addClass('shift');
                }
                info.find('.save').bind('click',function(){
                    document.location = '/download?basis_date='+refdate+'&dong_code='+opt.id;
                    return false;
                });
                info.find('.farm').bind('mouseenter',function(e){
                    var farm_code = $(this).attr('data-idx');
                    var target = $('[farm='+String(farm_code)+']');
                    var is_hot = $(this).parent().hasClass('hot');
                    $('#farm circle:last').after(target);
                    if(is_hot) target.attr('class','hot active');
                    else target.attr('class','active');
                }).bind('mouseleave',function(){
                    var farm_code = $(this).attr('data-idx');
                    var target = $('[farm='+String(farm_code)+']');
                    var is_hot = $(this).parent().hasClass('hot');
                    if(is_hot) target.attr('class','hot');
                    else target.removeAttr('class');
                });
            }
        },
        error : function(){
            closeInfo();
        }
    });
},

getRisks = function(option){
    if(spinner==null){
        $('<div id="spin"/>').appendTo(map);
        spinner = new Spinner(spinnerOption).spin(document.getElementById('spin'));
    }
    $.ajax({
        url : '/ajax/get/risks',
        type : 'get',
        data : {parent : option.parent, refdate : refdate},
        cache : true,
        success : function(d){
            risks = d;
            risk_dong = JSON.parse(JSON.stringify($region_dong));
            var geometries = risk_dong.objects.regions.geometries;
            var filtered = _.filter(geometries, function(v){ return _.findWhere(risks, {'dong_code' : Number(v.properties.DONG_CODE) }) != undefined });
            risk_dong.objects.regions.geometries = filtered;
            render(option);
        }
    });
},

getDongs = function(option){
    $('<div id="spin"/>').appendTo(map);
    spinner = new Spinner(spinnerOption).spin(document.getElementById('spin'));
    $.ajax({
        url : STATIC_URL+'region_dong.json',
        dataType : 'json',
        cache : true,
        success : function(r){
            $region_dong = r;
            getRisks(option);
        }
    });
},

initialize = function() {
    if(isMobile) {
        var viewport = document.querySelector("meta[name=viewport]").setAttribute('content', 'initial-scale=1.0, user-scalable=no');
    }
    mapWidth = $(window).width() - 656;
    mapHeight = $(window).height() - 84;
    projection = d3.geo.mercator().center([127, 36]).scale(5000).translate([mapWidth / 2, mapHeight / 2]);
    path = d3.geo.path().projection(projection).pointRadius(1.5);    
    $('#container,#info').css('height',mapHeight+'px');
    refdate = $('#s_year').val()+'-'+leadingZeros($('#s_month').val(),2)+'-'+leadingZeros($('#s_day').val(),2);
    getDongs({level:'si'});
    Binding();
    // $('#sl').bind('slider:changed',function(e,data){
    //     scale = data.value;
    //     var v = mapHeight * data.value;
    //     $('svg').attr({'height':v});
    // });
}

initialize();