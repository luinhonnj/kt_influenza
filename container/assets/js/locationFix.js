Window.prototype.lFix = function(id,x,y){
    switch(id){
        case 11 : x += -5; y += -15; break; //서울
        case 28 : x += 80; break; //인천
        case 41 : x += 20; y += 35; break; //경기
        case 42 : x += 20; break; //강원
        case 43 : x += -40; y += -35; break;//충북
        case 44 : x += -10; break; //충남
        case 45 : x += 20; y += -25; break;
        case 46 : x += -20; break; //전남
        case 47 : x += -70; break; //경북
        case 48 : x += -70; break; //경남
        case 4128 : x += -15; break; //고양시
        case 4148 : x += -30; break; //파주
        case 4157 : x += -40; break; //김포시
    }
    return {x:x,y:y};
}