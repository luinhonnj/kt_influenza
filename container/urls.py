from django.conf import settings
from django.contrib import admin
from django.conf.urls import patterns, include, url
from django.conf.urls.static import static
from django.views.generic.base import RedirectView

admin.autodiscover()

urlpatterns = patterns('',
    # Admin panel and documentation:
    (r'^grappelli/', include('grappelli.urls')), # grappelli URLS
    (r'^admin/',  include(admin.site.urls)), # admin site
    (r'^robots\.txt$', RedirectView.as_view(url=settings.STATIC_URL + 'robots.txt')),
)
urlpatterns+=static(settings.MEDIA_URL, document_root = settings.MEDIA_ROOT)
urlpatterns+=static(settings.STATIC_URL, document_root = settings.STATIC_ROOT)

urlpatterns+= patterns('apps.views',
    url(r'^$', 'index'),
    url(r'^ajax/get/risks$', 'get_risks'),
    url(r'^ajax/get/farms$', 'get_farms'),
    url(r'^ajax/get/estimate$', 'get_estimate'),
    url(r'^download$', 'download'),
)
