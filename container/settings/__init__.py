import socket

if socket.gethostname() in ['external','external-qa']:
    from prod import *
else:
    from dev import *
