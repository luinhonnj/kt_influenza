# -*- coding: utf-8 -*-
from os.path import join, normpath
from common import *

########## DEBUG CONFIGURATION
DEBUG = True
########## END DEBUG CONFIGURATION

########## EMAIL CONFIGURATION
EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'
########## END EMAIL CONFIGURATION