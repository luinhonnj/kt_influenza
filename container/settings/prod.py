# -*- coding: utf-8 -*-
from os.path import join, normpath
from common import *

########## DEBUG CONFIGURATION
DEBUG = False
########## END DEBUG CONFIGURATION

########## EMAIL CONFIGURATION
EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
########## END EMAIL CONFIGURATION

########## DATABASE CONFIGURATION
"""
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': '',
        'USER': 'root',
        'PASSWORD': 'nj120227!',
        'HOST': 'localhost',
        'OPTIONS': {
             "init_command": "SET foreign_key_checks = 0;",
        },
    }
}
"""
########## END DATABASE CONFIGURATION

########## COMPRESSION CONFIGURATION
COMPRESS_OFFLINE = False
COMPRESS_CSS_FILTERS += [
    'compressor.filters.cssmin.CSSMinFilter',
]
COMPRESS_JS_FILTERS += [
    'compressor.filters.jsmin.JSMinFilter',
]
########## END COMPRESSION CONFIGURATION
