from django.shortcuts import Http404
from django.utils.decorators import available_attrs
from functools import wraps
from container import settings

def ajax_required(function=None, method="POST", ajax_required=True):
    def decorator(view_func):
        @wraps(view_func, assigned=available_attrs(view_func))
        def _ajax_view(request, *args, **kwargs):
            if request.method != method and method != 'REQUEST':
                raise Http404
            if ajax_required and not request.is_ajax():
                raise Http404
            return view_func(request, *args, **kwargs)
        return _ajax_view

    if function:
        return decorator(function)
    return decorator