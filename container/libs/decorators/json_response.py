from django.http import HttpResponse
import datetime
import json

def json_success(data):
    json_message = {'success': True, 'data': data}
    return HttpResponse(json.dumps(json_message),"application/json")

def json_failure(error, reason):
    json_message = {'success': False, 'error':error, 'reason': reason}
    return HttpResponse(json.dumps(json_message),"application/json")